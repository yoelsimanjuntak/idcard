<section id="contact" class="gray-section contact" style="margin-top:  30px !important;">
    <div class="container">
        <div class="row m-b-lg">
            <div class="col-lg-12 text-center">
                <div class="navy-line"></div>
                <h1>Hubungi Kami</h1>
            </div>
        </div>
        <div class="row m-b-lg justify-content-center">
            <div class="col-lg-4">
                <address>
                    <strong><span class="navy">Badan Kesatuan Bangsa, Politik dan Perlindungan Masyarakat Kota Tebing Tinggi</span></strong><br/>
                    Jl. Gunung Agung Tebing Tinggi 20615<br/>
                    Tebing Tinggi, Sumatera Utara<br/>
                    <i class="fa fa-phone"></i> (0621) 325515&nbsp;&nbsp;<i class="fa fa-fax"></i> (0621) 325342
                </address>
                <div class="hr-line-dashed"></div>
                <iframe style="box-shadow: 2px 2px #dedede; border: solid 1px #dedede;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3982.988415764087!2d99.16201631426274!3d3.352975352802044!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3031604110206cd9%3A0xc9f75c7f136009c8!2sBadan+Kesatuan+Bangsa%2C+Politik+%26+Perlindungan+Masyarakat!5e0!3m2!1sen!2sid!4v1557277087962!5m2!1sen!2sid" width="360" height="440" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
            <div class="col-lg-4">
                <h4><span class="navy">Link Terkait</span></h4>
                <ul>
                    <li>
                        <a class="text-primary text-uppercase" href="http://portal.tebingtinggikota.go.id/">Website Pemerintah Kota Tebing Tinggi</a>
                    </li>
                    <li>
                        <a class="text-primary text-uppercase" href="http://portal.tebingtinggikota.go.id/">Facebook</a>
                    </li>
                    <li>
                        <a class="text-primary text-uppercase" href="http://portal.tebingtinggikota.go.id/">Instagram</a>
                    </li>
                </ul>
            </div>
            <div class="col-lg-4">
                <script type="text/javascript" src="https://widget.kominfo.go.id/gpr-widget-kominfo.min.js"></script>
                <div id="gpr-kominfo-widget-container"></div>
            </div>
        </div>
        <!--<div class="row">
            <div class="col-lg-12 text-center">
                <a href="mailto:test@email.com" class="btn btn-primary">Send us mail</a>
                <p class="m-t-sm">
                    Or follow us on social platform
                </p>
                <ul class="list-inline social-icon">
                    <li class="list-inline-item"><a href="#"><i class="fa fa-twitter"></i></a>
                    </li>
                    <li class="list-inline-item"><a href="#"><i class="fa fa-facebook"></i></a>
                    </li>
                    <li class="list-inline-item"><a href="#"><i class="fa fa-linkedin"></i></a>
                    </li>
                </ul>
            </div>-->
        </div>
        <div class="row">
            <div class="col-lg-12 text-center m-t-lg m-b-lg">
                <p><strong>&copy; <?=date('Y')?> <?=SITENAME?></strong><br/> Strongly developed by Partopi Tao</p>
            </div>
        </div>
    </div>
</section>

</div>
</div>

<!-- Mainly scripts -->
<script src="<?=base_url()?>assets/themes/inspinia/js/jquery-3.1.1.min.js"></script>
<script src="<?=base_url()?>assets/themes/inspinia/js/popper.min.js"></script>
<script src="<?=base_url()?>assets/themes/inspinia/js/bootstrap.js"></script>
<script src="<?=base_url()?>assets/themes/inspinia/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?=base_url()?>assets/themes/inspinia/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Flot -->
<script src="<?=base_url()?>assets/themes/inspinia/js/plugins/flot/jquery.flot.js"></script>
<script src="<?=base_url()?>assets/themes/inspinia/js/plugins/flot/jquery.flot.tooltip.min.js"></script>
<script src="<?=base_url()?>assets/themes/inspinia/js/plugins/flot/jquery.flot.spline.js"></script>
<script src="<?=base_url()?>assets/themes/inspinia/js/plugins/flot/jquery.flot.resize.js"></script>
<script src="<?=base_url()?>assets/themes/inspinia/js/plugins/flot/jquery.flot.pie.js"></script>

<!-- Peity -->
<script src="<?=base_url()?>assets/themes/inspinia/js/plugins/peity/jquery.peity.min.js"></script>
<script src="<?=base_url()?>assets/themes/inspinia/js/demo/peity-demo.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?=base_url()?>assets/themes/inspinia/js/inspinia.js"></script>
<script src="<?=base_url()?>assets/themes/inspinia/js/plugins/pace/pace.min.js"></script>
<script src="<?=base_url()?>assets/themes/inspinia/js/plugins/wow/wow.min.js"></script>

<!-- jQuery UI -->
<script src="<?=base_url()?>assets/themes/inspinia/js/plugins/jquery-ui/jquery-ui.min.js"></script>

<!-- GITTER -->
<script src="<?=base_url()?>assets/themes/inspinia/js/plugins/gritter/jquery.gritter.min.js"></script>

<!-- Sparkline -->
<script src="<?=base_url()?>assets/themes/inspinia/js/plugins/sparkline/jquery.sparkline.min.js"></script>

<!-- Sparkline demo data  -->
<script src="<?=base_url()?>assets/themes/inspinia/js/demo/sparkline-demo.js"></script>

<!-- ChartJS-->
<script src="<?=base_url()?>assets/themes/inspinia/js/plugins/chartJs/Chart.min.js"></script>

<!-- Toastr -->
<script src="<?=base_url()?>assets/themes/inspinia/js/plugins/toastr/toastr.min.js"></script>

<script src="<?=base_url()?>assets/themes/inspinia/js/plugins/slick/slick.min.js"></script>
<script>
    $(document).ready(function() {

        var inslider = $("#inSlider");
        if (inslider.length == 0) {
            $(".navbar-default").addClass('navbar-scroll')
        }
        else {
            $('body').scrollspy({
                target: '#navbar',
                offset: 80
            });
        }


        // Page scrolling feature
        $('a.page-scroll').bind('click', function(event) {
            var link = $(this);
            $('html, body').stop().animate({
                scrollTop: $(link.attr('href')).offset().top - 50
            }, 500);
            event.preventDefault();
            $("#navbar").collapse('hide');
        });
        $('a[href="<?=current_url()?>"]:not(.no-active)').addClass('active');
        $('a[href="<?=current_url()?>"]:not(.no-active)').parents('a').each(function() {
            $(this).addClass('active');
        });
        $('a[href="<?=current_url()?>"]:not(.no-active').parents('a').each(function() {
            $(this).addClass('in');
        });
    });

    var cbpAnimatedHeader = (function() {
        var docElem = document.documentElement,
            header = document.querySelector( '.navbar-default' ),
            didScroll = false,
            changeHeaderOn = 200;
        function init() {
            window.addEventListener( 'scroll', function( event ) {
                if( !didScroll ) {
                    didScroll = true;
                    setTimeout( scrollPage, 250 );
                }
            }, false );
        }
        function scrollPage() {
            var sy = scrollY();
            if ( sy >= changeHeaderOn ) {
                $(header).addClass('navbar-scroll')
            }
            else {
                $(header).removeClass('navbar-scroll')
            }
            didScroll = false;
        }
        function scrollY() {
            return window.pageYOffset || docElem.scrollTop;
        }
        var inslider = $("#inSlider");
        if (inslider.length > 0) {
            init();
        }


    })();

    // Activate WOW.js plugin for animation on scrol
    new WOW().init();
</script>
</body>