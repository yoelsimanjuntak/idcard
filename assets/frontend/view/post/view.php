<?php
/**
 * Created by PhpStorm.
 * User: Yoel Simanjuntak
 * Date: 15/10/2018
 * Time: 20:20
 */
?>
<?php $this->load->view('frontend/header') ?>
    <div class="ibox animated fadeInUp">
        <div class="ibox-content">
            <div class="float-right">
                <button class="btn btn-white btn-xs" type="button"><?=$data[COL_POSTCATEGORYNAME]?></button>
                <button class="btn btn-primary btn-xs" type="button"><i class="fa fa-eye"></i> <?=$data[COL_TOTALVIEW]?></button>
            </div>
            <div class="text-center article-title" style="margin: 40px 20px !important;">
                <h1>
                    <?=$data[COL_POSTTITLE]?>
                </h1>
                <?php
                $files = $this->db->where(COL_POSTID, $data[COL_POSTID])->get(TBL_POSTIMAGES)->result_array();
                ?>
                <div class="lightBoxGallery">
                    <?php
                if(count($files) > 1) {

                    foreach($files as $f) {
                        ?>
                        <a href="<?=MY_UPLOADURL.$f[COL_FILENAME]?>" title="<?=$data[COL_POSTTITLE]?>" data-gallery="">
                            <img src="<?=MY_UPLOADURL.$f[COL_FILENAME]?>" style="max-width: 20vh">
                        </a>
                    <?php
                    }
                } else if(!empty($files[0])) {
                    ?>
                    <a href="<?=MY_UPLOADURL.$files[0][COL_FILENAME]?>" title="<?=$data[COL_POSTTITLE]?>" data-gallery="">
                        <img src="<?=!empty($files[0][COL_FILENAME])?MY_UPLOADURL.$files[0][COL_FILENAME]:MY_NOIMAGEURL?>" alt="<?=$data[COL_POSTTITLE]?>" style="max-height: 60vh; max-width: 100%" />
                    </a>
                    <?php
                }
                    ?>
                </div>
                <?php
                ?>
            </div>
            <?=$data[COL_POSTCONTENT]?>
            <hr>
            <div class="small">
                <button class="btn btn-white btn-xs" type="button"><i class="fa fa-user"></i>&nbsp;&nbsp;<?=$data[COL_NAME]?></button>
                <button class="btn btn-white btn-xs" type="button"><i class="fa fa-clock-o"></i>&nbsp;&nbsp;<?=date("D, d M Y", strtotime($data[COL_CREATEDON]))?></button>
            </div>

            <div id="disqus_thread"></div>
            <script>

                /**
                 *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
                 *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
                /*
                */
                 var disqus_config = function () {
                 this.page.url = '<?=site_url('post/view/'.$data[COL_POSTSLUG])?>';  // Replace PAGE_URL with your page's canonical URL variable
                 this.page.identifier = '<?=$data[COL_POSTSLUG]?>'; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
                 };
                (function() { // DON'T EDIT BELOW THIS LINE
                    var d = document, s = d.createElement('script');
                    s.src = 'https://kesbangpol-tebing-tinggi.disqus.com/embed.js';
                    s.setAttribute('data-timestamp', +new Date());
                    (d.head || d.body).appendChild(s);
                })();
            </script>
            <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
        </div>
    </div>
<?php $this->load->view('frontend/footer') ?>