<?php
/**
 * Created by PhpStorm.
 * User: Yoel Simanjuntak
 * Date: 04/10/2018
 * Time: 13:58
 */
?>
<?php $this->load->view('frontend/header') ?>
    <div class="ibox animated fadeInUp">
        <div class="ibox-title">
            <h5>Berita</h5>
        </div>
        <div class="ibox-content">
            <?php
            if(count($data) > 0) {
                foreach($data as $n) {
                    ?>
                    <div class="faq-item">
                        <div class="row">
                            <div class="col-md-7">
                                <a data-toggle="collapse" href="#<?=$n[COL_POSTSLUG]?>" class="faq-question"><?=$n[COL_POSTTITLE]?></a>
                                <small><i class="fa fa-clock-o"></i> <?=date('D, d/m/Y H:i:s', strtotime($n[COL_CREATEDON]))?></small>
                            </div>
                            <div class="col-md-2">
                                <span class="small font-bold"><?=$n[COL_NAME]?></span>
                                <div class="tag-list">
                                    <span class="tag-item"><?=$n[COL_POSTCATEGORYNAME]?></span>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <a href="<?=site_url('post/view/'.$n[COL_POSTSLUG])?>" class="btn btn-white btn-sm">Lihat selengkapnya <i class="fa fa-arrow-right"></i></a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div id="<?=$n[COL_POSTSLUG]?>" class="panel-collapse collapse ">
                                    <div class="faq-answer">
                                        <?php
                                        $strippedcontent = strip_tags($n[COL_POSTCONTENT]);
                                        ?>
                                        <p style="text-align: justify">
                                            <?=strlen($strippedcontent) > 500 ? substr($strippedcontent, 0, 500) . "..." : $strippedcontent ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php
                }
            } else {
                ?>
                <p style="font-style: italic">Tidak ada data untuk ditampilkan.</p>
            <?php
            }
            ?>
        </div>
    </div>
<?php $this->load->view('frontend/footer') ?>