<?php

class MY_Controller extends CI_Controller{

    public $setting_org_name;
    public $setting_org_address;
    public $setting_org_lat;
    public $setting_org_long;
    public $setting_org_phone;
    public $setting_org_fax;
    public $setting_org_mail;

    public $setting_web_name;
    public $setting_web_desc;
    public $setting_web_logo;
    public $setting_web_disqus_url;
    public $setting_web_api_footerlink;
    public $setting_web_skin_class;
    public $setting_web_preloader;
    public $setting_web_version;

    function __construct(){
        parent::__construct();

        $this->setting_org_name = 'Dinas Komunikasi dan Informatika';
        $this->setting_org_address = 'Kabupaten Humbang Hasundutan';
        $this->setting_org_lat = '-';
        $this->setting_org_long = '-';
        $this->setting_org_phone = '-';
        $this->setting_org_fax = '-';
        $this->setting_org_mail = '-';
        $this->setting_web_name = 'ID Card Generator';
        $this->setting_web_desc = 'v1.0';
        $this->setting_web_disqus_url = '-';
        $this->setting_web_api_footerlink = '-';
        $this->setting_web_logo = 'logo.png';
        $this->setting_web_skin_class = '-';
        $this->setting_web_version = '1.0';
        $this->setting_web_preloader = 'loader-128x/Preloader_2.gif';
    }
}
