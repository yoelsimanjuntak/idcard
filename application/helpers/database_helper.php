<?php
define('TBL_ACTION','action');
define('TBL_PEGAWAI','pegawai');
define('TBL_PEGAWAI2','pegawai2');
define('TBL_USERLOGIN','userlogin');

define('COL_NO','no');
define('COL_NAMA','nama');
define('COL_ACTION','action');
define('COL_DETAIL','detail');
define('COL_TGLJAM','tgljam');
define('COL_NIP','nip');
define('COL_UNITKERJA','unitkerja');
define('COL_TEMPATLAHIR','tempatlahir');
define('COL_TGLLAHIR','tgllahir');
define('COL_PANGKAT-GOL-RUANG','pangkat-gol-ruang');
define('COL_TMTGOL','tmtgol');
define('COL_TMTCPNS','tmtcpns');
define('COL_JABATAN','jabatan');
define('COL_TMTJABATAN','tmtjabatan');
define('COL_DIKLATPENJENJANGAN','diklatpenjenjangan');
define('COL_TGLDIKLAT','tgldiklat');
define('COL_PENDIDIKANTERAKHIR','pendidikanterakhir');
define('COL_JURUSAN','jurusan');
define('COL_JENISKELAMIN','jeniskelamin');
define('COL_STATUSPNS','statuspns');
define('COL_MASAKERJA','masakerja');
define('COL_KETERANGAN','keterangan');
define('COL_SEKOLAH','sekolah');
define('COL_FOTO','foto');
define('COL_NIPLAMA','niplama');
define('COL_USIATAHUN','usiatahun');
define('COL_USIABULAN','usiabulan');
define('COL_PANGKAT','pangkat');
define('COL_LEVELJABATAN','leveljabatan');
define('COL_ALAMAT1','alamat1');
define('COL_NAMAKARTU','namakartu');
define('COL_NAMADALAMKARTU','namadalamkartu');
define('COL_USERNAME','username');
define('COL_PASSWORD','password');
define('COL_LEVEL','level');
define('COL_NAMALENGKAP','namalengkap');
define('COL_LASTLOGIN','lastlogin');
