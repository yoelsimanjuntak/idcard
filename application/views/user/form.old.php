<?php $this->load->view('header') ?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> <?= $title ?> <small> Form</small></h1>
        <ol class="breadcrumb">
            <li><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?=site_url('user/index')?>"> Users</a></li>
            <li class="active"><?=$edit?'Edit':'Add'?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-sm-12">
                <div class="box box-primary" style="border-top-color: transparent">
                    <div class="box-body">
                        <div style="display: none" class="alert alert-danger errorBox">
                            <i class="fa fa-ban"></i> Error :
                            <span class="errorMsg"></span>
                        </div>
                        <?php
                        if($this->input->get('error') == 1){
                            ?>
                            <div class="alert alert-danger alert-dismissible">
                                <i class="fa fa-ban"></i>
                                <span class="">Data gagal disimpan, silahkan coba kembali</span>
                            </div>
                            <?php
                        }
                        if(validation_errors()){
                            ?>
                            <div class="alert alert-danger alert-dismissible">
                                <i class="fa fa-ban"></i>
                                <?=validation_errors()?>
                            </div>
                            <?php
                        }
                        if(!empty($upload_errors)) {
                            ?>
                            <div class="alert alert-danger alert-dismissible">
                                <i class="fa fa-ban"></i>
                                <?=$upload_errors?>
                            </div>
                            <?php
                        }
                        ?>

                        <?=form_open_multipart(current_url(),array('role'=>'form','id'=>'userForm'))?>
                        <?php if(!$edit) {
                            ?>
                            <div class="col-sm-4">
                                <h4>Credentials</h4>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-user"></i></div>
                                        <input type="text" class="form-control" name="<?=COL_USERNAME?>" placeholder="Username" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-key"></i></div>
                                        <input type="password" class="form-control" name="<?=COL_PASSWORD?>" placeholder="Password" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-key"></i></div>
                                        <input type="password" class="form-control" name="RepeatPassword" placeholder="Repeat Password" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon">@</div>
                                        <input type="text" class="form-control" name="<?=COL_EMAIL?>" placeholder="Email" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <select name="<?=COL_ROLEID?>" class="form-control" required>
                                        <option value="">Select Role</option>
                                        <?=GetCombobox("SELECT * FROM roles", COL_ROLEID, COL_ROLENAME, (!empty($data[COL_ROLEID]) ? $data[COL_ROLEID] : null))?>
                                    </select>
                                </div>
                            </div>
                            <?php
                        } else {
                            ?>
                            <input type="hidden" name="<?=COL_ROLEID?>" value="<?=$data[COL_ROLEID]?>" disabled />
                        <?php
                        }
                        ?>

                        <div class="col-sm-8 user" style="padding: 0px;">
                            <div class="col-sm-6">
                                <h4>Informasi Pengguna</h4>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-user"></i></div>
                                        <input type="text" class="form-control required" name="<?=COL_NAME?>" value="<?=!empty($data[COL_NAME]) ? $data[COL_NAME] : ''?>" placeholder="Nama Lengkap">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-credit-card"></i></div>
                                        <input type="text" class="form-control" name="<?=COL_IDENTITYNO?>" value="<?=!empty($data[COL_IDENTITYNO]) ? $data[COL_IDENTITYNO] : ''?>" placeholder="No. Identitas">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-5">
                                        <label class="control-label">Jenis Kelamin</label>
                                    </div>
                                    <div class="col-sm-7" style="margin-bottom: 10px;">
                                        <label><input type="radio" name="<?=COL_GENDER?>" value="1" <?=(!empty($data[COL_GENDER]) ? ($data[COL_GENDER]==1?"checked":"") : "checked")?> /> &nbsp; Pria</label> &nbsp;&nbsp;
                                        <label><input type="radio" name="<?=COL_GENDER?>" value="2" <?=(!empty($data[COL_GENDER]) && $data[COL_GENDER]==2?"checked":"")?> /> &nbsp; Wanita</label> &nbsp;&nbsp;
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                        <input type="text" class="form-control datepicker" name="<?=COL_BIRTHDATE?>" value="<?=!empty($data[COL_BIRTHDATE]) ? date('d M Y', strtotime($data[COL_BIRTHDATE])) : ''?>" placeholder="Tanggal Lahir">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <h4>&nbsp;</h4>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-phone"></i></div>
                                        <input type="text" class="form-control" name="<?=COL_PHONENUMBER?>" value="<?=!empty($data[COL_PHONENUMBER]) ? $data[COL_PHONENUMBER] : ''?>" placeholder="No. Telepon">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" rows="4" placeholder="Alamat" name="<?=COL_ADDRESS?>"><?=!empty($data[COL_ADDRESS]) ? $data[COL_ADDRESS] : ''?></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-flat pull-right">Simpan</button>
                            </div>
                        </div>
                        <?=form_close()?>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php $this->load->view('loadjs') ?>
    <script type="text/javascript">
        $("[name=RoleID]").change(function() {
            var role = $(this).val();
            if(role == <?=ROLECOMPANY?>) {
                $(".user").fadeOut("slow", function() {
                    $(".company").fadeIn("slow", function(){
                        $("select", $(".company")).select2();
                        $(".required", $(".user")).attr("required", false);
                        $(".required", $(".company")).attr("required", true);
                    });
                });
            }
            else {
                $(".company").fadeOut("slow", function() {
                    $(".user").fadeIn("slow", function(){
                        $("select", $(".user")).select2();
                        $(".required", $(".user")).attr("required", true);
                        $(".required", $(".company")).attr("required", false);
                    });
                });
            }
            /*else {
                $(".company").fadeOut("slow", function() {});
                $(".user").fadeOut("slow", function(){});

                $(".required", $(".user")).attr("required", false);
                $(".required", $(".company")).attr("required", false);
            }*/
        }).trigger("change");
    </script>
<?php $this->load->view('footer') ?>