<?php
$this->load->view('header');
$ruser = GetLoggedUser();
?>
<style>
    .info-box-icon {
        height: 70px !important;
        line-height: 70px !important;
    }
    .info-box-icon-right {
        border-top-right-radius: .25rem;
        border-top-left-radius: 0;
        border-bottom-left-radius: 0;
        border-bottom-right-radius: .25rem;
        display: block;
        float: right;
        height: 70px !important;
        width: 24px;
        text-align: center;
        font-size: 12px;
        line-height: 70px;
        #background: rgba(0,0,0,0.1);
    }
    .info-box-icon-right:hover {
        background: rgba(0,0,0,0.15);
    }
    .info-box-icon-right>.small-box-footer {
        color: rgba(255,255,255,0.8);
    }
    .info-box-icon-right>.small-box-footer:hover {
        color: #fff;
    }
    .info-box .info-box-content {
        padding: 3px 10px !important;
    }
</style>
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Dashboard</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="breadcrumb-item active">Dashboard</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <?php
            if($ruser[COL_ROLEID] == ROLEADMIN) {
                ?>
                <div class="col-md-3">
                    <div class="info-box mb-3 bg-info">
                        <span class="info-box-icon"><i class="fas fa-users"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Users</span>
                            <span class="info-box-number"><?=number_format($count_user, 0)?></span>
                        </div>
                        <a class="info-box-icon-right" href="<?=site_url('post/index')?>" class="small-box-footer"><i class="fa fa-angle-right"></i></a>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="info-box mb-3 bg-success">
                        <span class="info-box-icon"><i class="fas fa-newspaper"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Posts</span>
                            <span class="info-box-number"><?=number_format($count_post, 0)?></span>
                        </div>
                        <a class="info-box-icon-right" href="<?=site_url('post/index')?>" class="small-box-footer"><i class="fa fa-angle-right"></i></a>
                    </div>
                </div>
                <?php
            } else {
                $r_employee = $this->db
                    ->join(TBL_MDEPARTMENT,TBL_MDEPARTMENT.'.'.COL_ID_DEPARTMENT." = ".TBL_MEMPLOYEE.".".COL_ID_DEPARTMENT,"left")
                    ->join(TBL_MPOSITION,TBL_MPOSITION.'.'.COL_ID_POSITION." = ".TBL_MEMPLOYEE.".".COL_ID_POSITION,"left")
                    ->where(COL_ID_EMPLOYEE, $ruser[COL_COMPANYID])
                    ->get(TBL_MEMPLOYEE)
                    ->row_array();
                ?>
                <div class="col-md-3">
                    <div class="info-box mb-3 bg-info">
                        <span class="info-box-icon"><i class="fa fa-black-tie"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text"><?=strtoupper($r_employee[COL_NM_EMPLOYEE])?></span>
                            <span class="info-box-number"><?=$r_employee[COL_NM_NIK]?></span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="info-box mb-3 bg-success">
                        <span class="info-box-icon"><i class="fas fa-building"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text text-sm"><?=strtoupper($r_employee[COL_NM_POSITION])?></span>
                            <span class="info-box-number text-sm"><?=strtoupper($r_employee[COL_NM_DEPARTMENT])?></span>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>
            <div class="col-md-3">
                <div class="info-box mb-3 bg-danger">
                    <span class="info-box-icon"><i class="fas fa-clipboard"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Projects</span>
                        <span class="info-box-number"><?=number_format($project_count, 0)?></span>
                    </div>
                    <a class="info-box-icon-right" href="<?=site_url('project/index')?>" class="small-box-footer"><i class="fa fa-angle-right"></i></a>
                </div>
            </div>
            <div class="col-md-3">
                <div class="info-box mb-3 bg-warning">
                    <span class="info-box-icon"><i class="fa fa-file-text-o"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Tasks</span>
                        <span class="info-box-number"><?=number_format($task_count, 0)?></span>
                    </div>
                    <a class="info-box-icon-right" href="<?=site_url('task/index')?>" class="small-box-footer"><i class="fa fa-angle-right"></i></a>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card card-outline card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Projects</h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="chart-responsive">
                                    <canvas id="pieChartProject" height="155" width="205" style="width: 205px; height: 155px;"></canvas>
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                    </div>
                    <!-- /.card-body -->
                    <!--<div class="card-footer">
                        <div class="row">
                            <div class="col-md-12">
                                <ul class="chart-legend clearfix text-sm" style="columns: 2; column-gap: 5px;">
                                    <?php
                                    foreach($project_data as $cat) {
                                        ?>
                                        <li><i class="far fa-circle" style="color: <?=$cat["color"]?>;"></i> <?=$cat["label"]?></li>
                                    <?php
                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </div>-->
                </div>
            </div>
            <div class="col-md-6">
                <div class="card card-outline card-danger">
                    <div class="card-header">
                        <h3 class="card-title">Tasks</h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="chart-responsive">
                                    <canvas id="pieChartTask" height="155" width="205" style="width: 205px; height: 155px;"></canvas>
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-md-12">
                                <ul class="chart-legend clearfix text-sm" style="columns: 2; column-gap: 5px;">
                                    <?php
                                    foreach($task_data as $cat) {
                                        ?>
                                        <li><i class="far fa-circle" style="color: <?=$cat["color"]?>;"></i> <?=$cat["label"]?></li>
                                    <?php
                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php $this->load->view('loadjs') ?>
<?php
$dataset = [];
$labels = [];
$rstatus = $this->db->where(COL_IS_PROJECT, 1)->get(TBL_MSTATUS)->result_array();
$rcategory = $this->db->get(TBL_MCATEGORY)->result_array();
foreach($rstatus as $s) {
    $project = array();
    foreach($rcategory as $cat) {
        $cproject = $this->db
            ->where(array(COL_ID_STATUS=>$s[COL_ID_STATUS], COL_ID_CATEGORY=>$cat[COL_ID_CATEGORY]))
            ->get(TBL_TPROJECT)
            ->num_rows();
        $project[] = $cproject;
    }
    $dataset[] = array(
        "label" => $s[COL_NM_STATUS],
        "fillColor" => $s[COL_NM_LABELCOLOR],
        "strokeColor" => $s[COL_NM_LABELCOLOR],
        "pointColor" => $s[COL_NM_LABELCOLOR],
        "pointStrokeColor" => $s[COL_NM_LABELCOLOR],
        "pointHighlightFill" => "#fff",
        "pointHighlightStroke" => $s[COL_NM_LABELCOLOR],
        "data" => $project
    );
}
foreach($rcategory as $cat) {
    $labels[] = $cat[COL_NM_CATEGORY];
}
?>
<script>

    //-------------
    //- PIE CHART -
    //-------------
    // Get context with jQuery - using jQuery's .get() method.
    /*var pieChartCanvas = $("#pieChartProject").get(0).getContext("2d");
    var pieChart = new Chart(pieChartCanvas);
    var PieData = <?=json_encode($project_data)?>;
    var pieOptions = {
        //Boolean - Whether we should show a stroke on each segment
        segmentShowStroke: true,
        //String - The colour of each segment stroke
        segmentStrokeColor: "#fff",
        //Number - The width of each segment stroke
        segmentStrokeWidth: 1,
        //Number - The percentage of the chart that we cut out of the middle
        percentageInnerCutout: 50, // This is 0 for Pie charts
        //Number - Amount of animation steps
        animationSteps: 100,
        //String - Animation easing effect
        animationEasing: "easeOutBounce",
        //Boolean - Whether we animate the rotation of the Doughnut
        animateRotate: true,
        //Boolean - Whether we animate scaling the Doughnut from the centre
        animateScale: false,
        //Boolean - whether to make the chart responsive to window resizing
        responsive: true,
        // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
        maintainAspectRatio: false,
        //String - A legend template
        legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>",
        //String - A tooltip template
        tooltipTemplate: "<%=label%> : <%=value %>"
    };
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    pieChart.Doughnut(PieData, pieOptions);*/
    //-----------------
    //- END PIE CHART -
    //-----------------

    //-------------
    //- PIE CHART -
    //-------------
    // Get context with jQuery - using jQuery's .get() method.
    var pieChartCanvas_pr = $("#pieChartTask").get(0).getContext("2d");
    var pieChart_pr = new Chart(pieChartCanvas_pr);
    var PieData_pr = <?=json_encode($task_data)?>;
    var pieOptions_pr = {
        //Boolean - Whether we should show a stroke on each segment
        segmentShowStroke: true,
        //String - The colour of each segment stroke
        segmentStrokeColor: "#fff",
        //Number - The width of each segment stroke
        segmentStrokeWidth: 1,
        //Number - The percentage of the chart that we cut out of the middle
        percentageInnerCutout: 50, // This is 0 for Pie charts
        //Number - Amount of animation steps
        animationSteps: 100,
        //String - Animation easing effect
        animationEasing: "easeOutBounce",
        //Boolean - Whether we animate the rotation of the Doughnut
        animateRotate: true,
        //Boolean - Whether we animate scaling the Doughnut from the centre
        animateScale: false,
        //Boolean - whether to make the chart responsive to window resizing
        responsive: true,
        // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
        maintainAspectRatio: false,
        //String - A legend template
        legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>",
        //String - A tooltip template
        tooltipTemplate: "<%=label%> : <%=value %>"
    };
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    pieChart_pr.Doughnut(PieData_pr, pieOptions_pr);
    //-----------------
    //- END PIE CHART -
    //-----------------

    var areaChartData = {
        labels: <?=json_encode($labels)?>,
        /*datasets: [
            {
                label: "Electronics",
                fillColor: "rgba(210, 214, 222, 1)",
                strokeColor: "rgba(210, 214, 222, 1)",
                pointColor: "rgba(210, 214, 222, 1)",
                pointStrokeColor: "#c1c7d1",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(220,220,220,1)",
                data: [65, 59, 80, 81, 56, 55, 40]
            },
            {
                label: "Digital Goods",
                fillColor: "rgba(60,141,188,0.9)",
                strokeColor: "rgba(60,141,188,0.8)",
                pointColor: "#3b8bba",
                pointStrokeColor: "rgba(60,141,188,1)",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(60,141,188,1)",
                data: [28, 48, 40, 19, 86, 27, 90]
            }
        ]*/
        datasets: <?=json_encode($dataset)?>
    };

    //-------------
    //- BAR CHART -
    //-------------
    var barChartCanvas = $("#pieChartProject").get(0).getContext("2d");
    var barChart = new Chart(barChartCanvas);
    var barChartData = areaChartData;
    var barChartOptions = {
        //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
        scaleBeginAtZero: true,
        //Boolean - Whether grid lines are shown across the chart
        scaleShowGridLines: true,
        //String - Colour of the grid lines
        scaleGridLineColor: "rgba(0,0,0,.05)",
        //Number - Width of the grid lines
        scaleGridLineWidth: 1,
        //Boolean - Whether to show horizontal lines (except X axis)
        scaleShowHorizontalLines: true,
        //Boolean - Whether to show vertical lines (except Y axis)
        scaleShowVerticalLines: true,
        //Boolean - If there is a stroke on each bar
        barShowStroke: true,
        //Number - Pixel width of the bar stroke
        barStrokeWidth: 2,
        //Number - Spacing between each of the X value sets
        barValueSpacing: 5,
        //Number - Spacing between data sets within X values
        barDatasetSpacing: 1,
        //String - A legend template
        legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
        //Boolean - whether to make the chart responsive
        responsive: true,
        maintainAspectRatio: true
    };

    barChartOptions.datasetFill = false;
    barChart.Bar(barChartData, barChartOptions);
</script>
<?php $this->load->view('footer') ?>

