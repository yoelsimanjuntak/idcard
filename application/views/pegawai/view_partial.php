<?php
/**
 * Created by PhpStorm.
 * User: Partopi Tao
 * Date: 01/02/2020
 * Time: 20:03
 */
$displaypic = MY_IMAGEURL.'pegawai/blank.jpg';
if(!empty($res[COL_NIP]) && is_file(MY_IMAGEPATH.'pegawai/'.$res[COL_NIP].'.jpg')) {
    $displaypic = MY_IMAGEURL.'pegawai/'.$res[COL_NIP].'.jpg';
}
?>
<style>
    #img-detail {
        z-index: 5;
        height: 90px;
        width: 90px;
        border: 3px solid;
        border-color: rgba(255,255,255,.2);
    }
</style>
<div class="row">

</div>
<div class="row">
    <div class="col-5 col-sm-3">
        <div class="nav flex-column nav-tabs" id="vert-tabs-tab" role="tablist" aria-orientation="vertical">
            <div class="col-sm-12 mb-3 text-center">
                <img src="<?=$displaypic?>" id="img-detail" class="img-circle elevation-2" style="border-radius: 10%" alt="<?=$res[COL_NAMA]?>">
            </div>
            <a class="nav-link active" id="vert-tabs-bio-tab" data-toggle="pill" href="#vert-tabs-bio" role="tab" aria-controls="vert-tabs-bio" aria-selected="true">Biodata</a>
            <a class="nav-link" id="vert-tabs-emp-tab" data-toggle="pill" href="#vert-tabs-emp" role="tab" aria-controls="vert-tabs-emp" aria-selected="false">Kepegawaian</a>
        </div>
    </div>
    <div class="col-7 col-sm-9">
        <div class="tab-content" id="vert-tabs-tabContent">
            <div class="tab-pane fade active show" id="vert-tabs-bio" role="tabpanel" aria-labelledby="vert-tabs-bio-tab">
                <table id="detail" class="table table-striped">
                    <tbody>
                    <tr>
                        <td>Nama</td>
                        <td>:</td>
                        <td><?=$res[COL_NAMA]?></td>
                    </tr>
                    <tr>
                        <td>Tempat & Tgl. Lahir</td>
                        <td>:</td>
                        <td><?=$res[COL_TEMPATLAHIR].', '.date('d-m-Y', strtotime($res[COL_TGLLAHIR]))?></td>
                    </tr>
                    <tr>
                        <td>Jenis Kelamin</td>
                        <td>:</td>
                        <td><?=$res[COL_JENISKELAMIN]?></td>
                    </tr>
                    <tr>
                        <td>Alamat</td>
                        <td>:</td>
                        <td><?=$res[COL_ALAMAT1]?></td>
                    </tr>
                    <tr>
                        <td>Pendidikan Terakhir</td>
                        <td>:</td>
                        <td><?=$res[COL_PENDIDIKANTERAKHIR]?></td>
                    </tr>
                    <tr>
                        <td>Sekolah</td>
                        <td>:</td>
                        <td><?=$res[COL_SEKOLAH]?></td>
                    </tr>
                    <tr>
                        <td>Jurusan</td>
                        <td>:</td>
                        <td><?=$res[COL_JURUSAN]?></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="tab-pane fade" id="vert-tabs-emp" role="tabpanel" aria-labelledby="vert-tabs-emp-tab">
                <table id="detail" class="table table-striped">
                    <tbody>
                    <tr>
                        <td>Nama di Kartu</td>
                        <td>:</td>
                        <td><?=$res[COL_NAMAKARTU]?></td>
                    </tr>
                    <tr>
                        <td>NIP / NIK</td>
                        <td>:</td>
                        <td><?=$res[COL_NIP]?></td>
                    </tr>
                    <tr>
                        <td>Status Kepegawaian</td>
                        <td>:</td>
                        <td><?=$res[COL_STATUSPNS]?></td>
                    </tr>
                    <tr>
                        <td>Unit Kerja</td>
                        <td>:</td>
                        <td><?=$res[COL_UNITKERJA]?></td>
                    </tr>
                    <tr>
                        <td>Jabatan</td>
                        <td>:</td>
                        <td><?=$res[COL_JABATAN]?></td>
                    </tr>
                    <?php
                    if ((strpos(strtolower($res[COL_STATUSPNS]), 'cpns') !== false) || (strpos(strtolower($res[COL_STATUSPNS]), 'pns') !== false)) {
                        ?>
                        <tr>
                            <td>Pangkat / Gol. Ruang</td>
                            <td>:</td>
                            <td><?=$res['pangkat-gol-ruang']?></td>
                        </tr>
                        <tr>
                            <td>TMT. Golongan</td>
                            <td>:</td>
                            <td><?=date('d-m-Y', strtotime($res[COL_TMTGOL]))?></td>
                        </tr>
                        <tr>
                            <td>TMT. Jabatan</td>
                            <td>:</td>
                            <td><?=date('d-m-Y', strtotime($res[COL_TMTJABATAN]))?></td>
                        </tr>
                        <?php
                    }
                    ?>
                    <tr>
                        <td>Masa Kerja</td>
                        <td>:</td>
                        <td><?=$res[COL_MASAKERJA]?></td>
                    </tr>
                    <tr>
                        <td>Keterangan</td>
                        <td>:</td>
                        <td><?=$res[COL_KETERANGAN]?></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>