<?php
/**
 * Created by PhpStorm.
 * User: Partopi Tao
 * Date: 01/02/2020
 * Time: 21:52
 */

$depan = $this->load->view('pegawai/view_cetak_partial', array('res'=>$res, 'side'=>'depan'), true);
$belakang = $this->load->view('pegawai/view_cetak_partial', array('res'=>$res, 'side'=>'belakang'), true);
$this->load->library('Mypdf');
$mpdf = new \mPDF('c', array(55,85), '', '', 0, 0, 0, 0, 0, 0);
$mpdf->SetTitle('Cetak Kartu | '.$res[COL_NIP]);
$mpdf->SetDisplayMode('fullpage');
$mpdf->simpleTables = true;
$mpdf->packTableData = true;
$keep_table_proportions = TRUE;
$mpdf->shrink_tables_to_fit=1;
$mpdf->WriteHTML($depan);
$mpdf->AddPage();
$mpdf->WriteHTML($belakang);
$mpdf->Output('Kartu ID '.$res[COL_NIP].'.pdf', 'I');