<?php
/**
 * Created by PhpStorm.
 * User: Partopi Tao
 * Date: 01/02/2020
 * Time: 21:46
 */
$CI =& get_instance();
$CI->load->library('ciqrcode');
$config['cacheable']	= false; //boolean, the default is true
$config['cachedir']		= ''; //string, the default is application/cache/
$config['errorlog']		= ''; //string, the default is application/logs/
$config['quality']		= true; //boolean, the default is true
$config['size']			= ''; //interger, the default is 1024
$config['black']		= array(224,255,255); // array, default is array(255,255,255)
$config['white']		= array(70,130,180); // array, default is array(0,0,0)

$linkpegawai = site_url('home/pegawai-view/'.$res[COL_NO]);
$fotopegawai=MY_IMAGEPATH.'pegawai/'.$res[COL_NIP].".jpg";
$filenip = MY_IMAGEPATH.'pegawai/'.$res[COL_NIP].".png";

$params['data'] = $linkpegawai;
$params['level'] = 'L';
$params['size'] = 4;
$params['savename'] = $filenip;

if (!file_exists($filenip)) {
    $CI->ciqrcode->generate($params);
}
?>
<style>
    body, p {
        font-size: 10px;
        font-family: "Gill Sans MT", "Myriad Pro", "DejaVu Sans Condensed", Helvetica, Arial, sans-serif;
    }
    .text-small {
        font-size: 9.5px !important;
    }
    #belakang {
        font-size: 7px !important;
    }
    .sign {
        background-image: url(<?=MY_IMAGEURL.'pegawai/_sign.jpeg'?>);
        background-image-resize: 3;
        background-repeat: no-repeat;
        background-position-x: right;
        background-position-y: bottom;
    }
    .card-front-bottom {
        background-image: url(<?=MY_IMAGEURL.'pegawai/_card_bottom.png'?>);
        background-image-resize: 6;
        background-repeat: no-repeat;
        background-position-x: right;
        background-position-y: bottom;
    }
</style>
<?php
if($side == 'depan') {
    ?>
    <table border="0" autosize="1" border="0" width="100%" style="overflow: wrap">
        <tr>
            <td>
                <img src="<?=MY_IMAGEURL.'pegawai/_card_top.png'?>" width="100%" />
            </td>
        </tr>
        <tr>
            <td style="text-align: center; font-weight: bold; max-width: 200px; background-color: #ff5722; color: #fff;">
                <p><?= strtoupper($res[COL_UNITKERJA]); ?></p>
            </td>
        </tr>
        <tr>
            <td style="text-align: center" class="card-front-bottom">
                <div style="margin-top: 10px">
                    <br />
                    <img src="<?php if (file_exists($fotopegawai)) {echo $fotopegawai;} else {echo MY_IMAGEURL.'pegawai/blank.jpg'; } ?>" width="140px" height="185px"/>
                </div>
            </td>
        </tr>
        <tr>
            <td style="text-align: center; font-weight: bold; max-width: 200px; padding: 0px 10px">
                <p style="font-size: 13px !important"><?= $res[COL_NAMAKARTU]; ?></p>
            </td>
        </tr>
    </table>
    <?php
} else if($side == 'belakang') {
    ?>
    <br /><br />
    <table border="0" width="100%" id="belakang">
        <tr>
            <td style="text-align: center; background-color:#CBCBCB; border-radius:2px; width: 200px">
                <p style="font-weight: bold">IDENTITAS PEGAWAI</p>
            </td>
        </tr>
        <tr>
            <td>
                <table style="margin-top: 10px; font-size: 8px">
                    <tr>
                        <td><?=$res[COL_STATUSPNS]=='Kontrak'?'NIK':'NIP'?></td><td>:</td><td><?=$res[COL_NIP]?></td>
                    </tr>

                    <tr>
                        <td>Tempat Tgl Lahir</td><td>:</td><td><?=$res[COL_TEMPATLAHIR].','.$res[COL_TGLLAHIR]?></td>
                    </tr>
                    <tr>
                        <td style="white-space: nowrap;">Jenis Kelamin</td><td>:</td><td><?=$res[COL_JENISKELAMIN]?></td>
                    </tr>
                    <?php
                    if($res[COL_STATUSPNS]!='Kontrak') {
                        ?>
                        <tr>
                            <td>Pangkat/Gol</td><td>:</td><td><?=$res['pangkat-gol-ruang']?></td>
                        </tr>
                        <?php
                    }
                    ?>
                    <tr>
                        <td>Jabatan</td><td>:</td><td><?=$res[COL_JABATAN]?></td>
                    </tr>
                    <?php
                    if($res[COL_STATUSPNS]=='Kontrak') {
                        ?>
                        <tr>
                            <td>Masa Kontrak</td><td>:</td><td><?=$res['masakerja']?></td>
                        </tr>
                        <?php
                    }
                    ?>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table style="margin-top: 20px;">
                    <tr class="text-xsmall">
                        <td style="width: 60px">
                            <img src="<?=$filenip?>" width="61px" height="61px"/>
                        </td>
                        <td class="sign">
                            a.n BUPATI HUMBANG HASUNDUTAN<br />
                            SEKRETARIS DAERAH KABUPATEN,<br />
                            <br /><br />
                            Drs. TONNY SIHOMBING, M.IP<br />
                            PEMBINA UTAMA MADYA<br />
                            NIP 19631030 199203 1 003
                        </td>
                    </tr>
                    <tr style="font-size: 8px">
                        <td colspan="2">
                            <table style="text-align: justify">
                                <tr>
                                    <td colspan="2">Catatan</td>
                                </tr>
                                <tr><td valign="top">1.</td><td>Kartu Identitas harus dipakai selama jam kerja / waktu dinas luar.</td></tr>
                                <tr><td valign="top">2.</td><td>Kartu Identitas ini milik Pemkab Humbang Hasundutan.</td></tr>
                                <tr><td valign="top">3.</td><td>Jika kartu ini tercecer harap dikembalikan ke Dinas Komunikasi dan Informatika Kab. Humbang Hasundutan.</td></tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <?php
}
?>
