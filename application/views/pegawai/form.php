<?php
/**
 * Created by PhpStorm.
 * User: Partopi Tao
 * Date: 01/02/2020
 * Time: 15:06
 */
$this->load->view('header') ?>
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark"><?= $title ?></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?=site_url()?>"><i class="fa fa-home"></i> Home</a></li>
                    <li class="breadcrumb-item active"><?=$title?></li>
                </ol>
            </div>
        </div>
    </div>
</div>
<section class="content">
    <div class="container-fluid">
        <?php
        $this->load->view('pegawai/form_partial');
        ?>
    </div>
</section>
<?php $this->load->view('footer') ?>