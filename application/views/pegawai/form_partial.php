<?php
/**
 * Created by PhpStorm.
 * User: Partopi Tao
 * Date: 01/02/2020
 * Time: 15:12
 */
?>
<div class="row">
    <div class="col-sm-12">
        <div class="card card-primary">
            <?=form_open_multipart(current_url(),array('role'=>'form','id'=>'main-form','class'=>'form-horizontal'))?>
            <div class="card-body">
                <div style="display: none" class="alert alert-danger errorBox">
                    <i class="fa fa-ban"></i> Error :
                    <span class="errorMsg"></span>
                </div>
                <?php
                if($this->input->get('error') == 1){
                    ?>
                    <div class="alert alert-danger alert-dismissible">
                        <i class="fa fa-ban"></i>
                        <span class="">Data gagal disimpan, silahkan coba kembali</span>
                    </div>
                <?php
                }
                if(validation_errors()){
                    ?>
                    <div class="alert alert-danger alert-dismissible">
                        <i class="fa fa-ban"></i>
                        <?=validation_errors()?>
                    </div>
                <?php
                }
                if(!empty($upload_errors)) {
                    ?>
                    <div class="alert alert-danger alert-dismissible">
                        <i class="fa fa-ban"></i>
                        <?=$upload_errors?>
                    </div>
                <?php
                }
                ?>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group row">
                            <label class="control-label col-sm-4">Nama</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" placeholder="Isi Nama Lengkap dan Gelar" name="<?=COL_NAMA?>" value="<?= $edit ? $data[COL_NAMA] : ""?>" required />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-sm-4">Status Kepegawaian</label>
                            <div class="col-sm-8">
                                <select name="<?=COL_STATUSPNS?>" class="form-control">
                                    <option value="PNS" <?=!empty($data)&&$data[COL_STATUSPNS]=='PNS'?'selected':''?>>PNS</option>
                                    <option value="CPNS" <?=!empty($data)&&$data[COL_STATUSPNS]=='CPNS'?'selected':''?>>CPNS</option>
                                    <option value="Kontrak" <?=!empty($data)&&$data[COL_STATUSPNS]=='Kontrak'?'selected':''?>>Kontrak</option>
                                    <option value="Ka. Daerah" <?=!empty($data)&&$data[COL_STATUSPNS]=='Ka. Daerah'?'selected':''?>>Kepala Daerah</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-sm-4">NIP / NIK</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" placeholder="xxxxxxxx xxxxxx x xxx" name="<?=COL_NIP?>" value="<?= $edit ? $data[COL_NIP] : ""?>" required />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-sm-4">Tempat / Tgl. Lahir</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" placeholder="Tempat Lahir" name="<?=COL_TEMPATLAHIR?>" value="<?= $edit ? $data[COL_TEMPATLAHIR] : ""?>" />
                            </div>
                            <div class="col-sm-4">
                                <input type="text" class="form-control datepicker" placeholder="Tanggal Lahir" name="<?=COL_TGLLAHIR?>" value="<?= $edit ? $data[COL_TGLLAHIR] : ""?>" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-sm-4">Jenis Kelamin</label>
                            <div class="col-sm-8">
                                <div class="icheck-primary d-inline">
                                    <input type="radio" id="radioJK_LK" value="Laki-Laki" name="<?=COL_JENISKELAMIN?>" <?=!empty($data)&&$data[COL_JENISKELAMIN]=='Laki-Laki'?'checked':(empty($data)?'checked':'')?> />
                                    <label for="radioJK_LK">
                                        Laki-Laki
                                    </label>
                                </div>
                                &nbsp;&nbsp;
                                <div class="icheck-primary d-inline">
                                    <input type="radio" id="radioJK_PR" value="Perempuan" name="<?=COL_JENISKELAMIN?>" <?=!empty($data)&&$data[COL_JENISKELAMIN]=='Perempuan'?'checked':''?> />
                                    <label for="radioJK_PR">
                                        Perempuan
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-sm-4">Alamat</label>
                            <div class="col-sm-8">
                                <textarea name="<?=COL_ALAMAT1?>" class="form-control" rows="3"><?= $edit ? $data[COL_ALAMAT1] : ""?></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-sm-4">Pendidikan Terakhir</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" placeholder="SD / SMP / SMA / S1 / S2 / dst" name="<?=COL_PENDIDIKANTERAKHIR?>" value="<?= $edit ? $data[COL_PENDIDIKANTERAKHIR] : ""?>" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-sm-4">Sekolah</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" placeholder="Sekolah / Perguruan Tinggi / Universitas" name="<?=COL_SEKOLAH?>" value="<?= $edit ? $data[COL_SEKOLAH] : ""?>" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-sm-4">Jurusan</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" placeholder="Jurusan" name="<?=COL_JURUSAN?>" value="<?= $edit ? $data[COL_JURUSAN] : ""?>" />
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 pl-3">
                        <div class="form-group row">
                            <label class="control-label col-sm-4">Nama Kartu</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" placeholder="Isi Nama Dalam Kartu" name="<?=COL_NAMAKARTU?>" value="<?= $edit ? $data[COL_NAMAKARTU] : ""?>" required />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-sm-4">Unit Kerja</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" placeholder="Dinas / Badan / Kantor / UPT" name="<?=COL_UNITKERJA?>" value="<?= $edit ? $data[COL_UNITKERJA] : ""?>" required />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-sm-4">Jabatan</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" placeholder="Jabatan" name="<?=COL_JABATAN?>" value="<?= $edit ? $data[COL_JABATAN] : ""?>" />
                            </div>
                        </div>
                        <div class="form-group row input-pns-only">
                            <label class="control-label col-sm-4">Pangkat / Gol. Ruang</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" placeholder="[Pangkat], [Gol. Ruang]" name="pangkat-gol-ruang" value="<?= $edit ? $data["pangkat-gol-ruang"] : ""?>" />
                            </div>
                        </div>
                        <div class="form-group row input-pns-only">
                            <label class="control-label col-sm-4">TMT. CPNS</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control datepicker" placeholder="TMT. CPNS" name="<?=COL_TMTCPNS?>" value="<?= $edit ? $data[COL_TMTCPNS] : ""?>" />
                            </div>
                        </div>
                        <div class="form-group row input-pns-only">
                            <label class="control-label col-sm-4">TMT. Golongan</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control datepicker" placeholder="TMT. Golongan" name="<?=COL_TMTGOL?>" value="<?= $edit ? $data[COL_TMTGOL] : ""?>" />
                            </div>
                        </div>
                        <div class="form-group row input-pns-only">
                            <label class="control-label col-sm-4">TMT. Jabatan</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control datepicker" placeholder="TMT. Golongan" name="<?=COL_TMTGOL?>" value="<?= $edit ? $data[COL_TMTGOL] : ""?>" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-sm-4">Masa Kerja</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" placeholder="Masa Kerja" name="<?=COL_MASAKERJA?>" value="<?= $edit ? $data[COL_MASAKERJA] : ""?>" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-sm-4">Keterangan</label>
                            <div class="col-sm-8">
                                <textarea name="<?=COL_KETERANGAN?>" class="form-control" rows="3"><?= $edit ? $data[COL_KETERANGAN] : ""?></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <div class="row">
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-primary">SUBMIT</button>
                    </div>
                </div>
            </div>
            <?=form_close()?>
        </div>
    </div>
</div>
<?php $this->load->view('loadjs') ?>
<script>
    $(document).ready(function() {
        $("[name=statuspns]").change(function() {
            var val = $(this).val();
            if(val == 'PNS' || val == 'CPNS') {
                $('.input-pns-only').show();
            } else {
                $('.input-pns-only').hide();
            }
        }).trigger('change');
    });
</script>