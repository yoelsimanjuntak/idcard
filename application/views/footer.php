</div>
<footer class="main-footer">
    <div class="float-right d-none d-sm-inline">
        <b>Version</b> <?=$this->setting_web_version?>
    </div>
    <strong>Copyright &copy; <?=date("Y")?> <?=$this->setting_web_name?></strong>. Strongly developed by <b>Partopi Tao</b>.
</footer>
</div>
</body>
</html>