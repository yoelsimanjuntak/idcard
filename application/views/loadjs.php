<!-- Bootstrap -->
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- overlayScrollbars -->
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="<?=base_url()?>assets/themes/adminlte-new/dist/js/adminlte.js"></script>

<!-- jQuery Mapael -->
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/jquery-mousewheel/jquery.mousewheel.js"></script>
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/raphael/raphael.min.js"></script>
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/jquery-mapael/jquery.mapael.min.js"></script>
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/jquery-mapael/maps/world_countries.min.js"></script>
<!-- ChartJS -->
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/chart.js.old/Chart.min.js"></script>

<!-- FastClick -->
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/fastclick/fastclick.js"></script>
<!-- Sparkline -->
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/sparklines/sparkline.js"></script>
<!-- SlimScroll 1.3.0 -->
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/iCheck/icheck.min.js"></script>
<!-- Select 2 -->
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/select2/js/select2.full.min.js"></script>
<!-- Bootstrap select -->
<script src="<?=base_url()?>assets/js/bootstrap-select.js"></script>
<!-- Upload file -->
<!--<script src="<?=base_url()?>assets/js/jquery.uploadfile.min.js"></script>-->

<!-- Block UI -->
<script type="text/javascript" src="<?=base_url() ?>assets/js/jquery.blockUI.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/template/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/template/js/function.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/template/js/jquery.form.js"></script>

<!-- Bootstrap WYSIHTML5 -->
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>

<!-- CK Editor -->
<script src="<?=base_url()?>assets/js/ckeditor/ckeditor.js"></script>

<!-- date-range-picker -->
<script src="<?=base_url()?>assets/js/moment.js"></script>
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/daterangepicker/daterangepicker.js"></script>

<!-- bootstrap color picker -->
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>

<!-- JSPDF -->
<script src="<?=base_url()?>assets/js/jspdf/jspdf.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/jspdf/libs/png_support/zlib.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/jspdf/libs/png_support/png.js"></script>
<script src="<?=base_url()?>assets/js/jspdf/plugins/from_html.js"></script>
<script src="<?=base_url()?>assets/js/jspdf/plugins/addimage.js"></script>
<script src="<?=base_url()?>assets/js/jspdf/plugins/png_support.js"></script>
<script src="<?=base_url()?>assets/js/jspdf/plugins/split_text_to_size.js"></script>
<script src="<?=base_url()?>assets/js/jspdf/plugins/standard_fonts_metrics.js"></script>
<script src="<?=base_url()?>assets/js/jspdf/plugins/filesaver.js"></script>

<!-- Toastr -->
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/toastr/toastr.min.js"></script>

<!-- HTML2CANVAS -->
<script src="<?=base_url()?>assets/js/html2canvas.js"></script>

<script type="text/javascript" src="<?=base_url()?>assets/js/jquery.number.js"></script>

<!--<script type="text/javascript" src="--><?//=base_url()?><!--assets/tbs/js/plugins/datatables/jquery.dataTables.js"></script>-->
<!--<script type="text/javascript" src="--><?//=base_url()?><!--assets/tbs/js/plugins/datatables/dataTables.bootstrap.js"></script>-->
<!--<script type="text/javascript" src="--><?//=base_url()?><!--assets/tbs/js/plugins/datatables/ColReorderWithResize.js"></script>-->
<!--<script type="text/javascript" src="--><?//=base_url()?><!--assets/js/bootstrap-multiselect.js"></script>-->

<script>
    Date.prototype.setISO8601 = function (string) {
        var regexp = "([0-9]{4})(-([0-9]{2})(-([0-9]{2})" +
            "(T([0-9]{2}):([0-9]{2})(:([0-9]{2})(\.([0-9]+))?)?" +
            "(Z|(([-+])([0-9]{2}):([0-9]{2})))?)?)?)?";
        var d = string.match(new RegExp(regexp));

        var offset = 0;
        var date = new Date(d[1], 0, 1);

        if (d[3]) { date.setMonth(d[3] - 1); }
        if (d[5]) { date.setDate(d[5]); }
        if (d[7]) { date.setHours(d[7]); }
        if (d[8]) { date.setMinutes(d[8]); }
        if (d[10]) { date.setSeconds(d[10]); }
        if (d[12]) { date.setMilliseconds(Number("0." + d[12]) * 1000); }
        if (d[14]) {
            offset = (Number(d[16]) * 60) + Number(d[17]);
            offset *= ((d[15] == '-') ? 1 : -1);
        }

        offset -= date.getTimezoneOffset();
        time = (Number(date) + (offset * 60 * 1000));
        this.setTime(Number(time));
    };

    function DuaDigit(x){
        x = x.toString();
        var len = x.length;
        if(len < 2){
            return "0"+x;
        }else{
            return x;
        }
    }

    var mymodal;
    function UseModal(){
        var modalcontent = '<div class="modal fade" id="generalModal" tabindex="-1" role="dialog" aria-hidden="true">'+
            '<div class="modal-dialog modal-lg">'+
            '<div class="modal-content">'+
            '<div class="modal-header">'+
            '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>'+
            '<h4 class="modal-title" id="myModalLabel">Modal title</h4>'+
            '</div>'+
            '<div class="modal-body">'+
            '</div>'+
            '<div class="modal-footer">'+
            '<button type="button" class="btn btn-default modalCancel" data-dismiss="modal">Cancel</button>'+
            '<button type="button" class="btn btn-primary modalOK">OK</button>'+
            '</div>'+
            '</div>'+
            '</div>';
        if(!$('#generalModal').length){
            $(modalcontent).appendTo('body');
            mymodal = $('#generalModal');
        }
    }

    function LoadModal(url,data,cb){
        mymodal.find('.modal-body').empty().load(url,data,cb);
    }

    function ModalTitle(title){
        mymodal.find('#myModalLabel').empty().text(title);
    }

    function CloseModal(){
        mymodal.modal('hide');
    }

    function timeConverter(UNIX_timestamp){
        var a = new Date(UNIX_timestamp*1000);
        var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
        var year = a.getFullYear();
        var month = months[a.getMonth()];
        var date = a.getDate();
        var hour = a.getHours();
        var min = a.getMinutes();
        var sec = a.getSeconds();
        var time = DuaDigit(date)+' '+month+' '+year+' '+DuaDigit(hour)+':'+DuaDigit(min)+':'+DuaDigit(sec);
        return time;
    }

    $(document).ready(function(){
        $('.ui').button();
        $('a[href="<?=current_url()?>"]').addClass('active');
        $('.dropdown-menu textarea,.dropdown-menu input, .dropdown-menu label').click(function(e) {
            e.stopPropagation();
        });
        $(document).on('keypress','.angka',function(e){
            if((e.which <= 57 && e.which >= 48) || (e.keyCode >= 37 && e.keyCode <= 40) || e.keyCode==9 || e.which==43 || e.which==44 || e.which==45 || e.which==46 || e.keyCode==8){
                return true;
            }else{
                return false;
            }
        });
        $(document).on('blur','.uang',function(){
            $(this).val(desimal($(this).val(),0));
        }).on('focus','.uang',function(){
            $(this).val(toNum($(this).val()));
        });
        $(".uang").trigger("blur");

        if($('.cekboxaction').length){
            $('.cekboxaction').click(function(){
                var a = $(this);
                var confirmDialog = $("#confirmDialog");
                var alertDialog = $("#alertDialog");

                confirmDialog.on("hidden.bs.modal", function(){
                    $(".modal-body", confirmDialog).html("");
                });
                alertDialog.on("hidden.bs.modal", function(){
                    $(".modal-body", alertDialog).html("");
                });

                if($('.cekbox:checked').length < 1){
                    $(".modal-body", alertDialog).html("No data selected.");
                    alertDialog.modal("show");
                    //alert('Tidak ada data dipilih');
                    return false;
                }

                $(".modal-body", confirmDialog).html("Are you sure?");
                confirmDialog.modal("show");
                $(".btn-ok", confirmDialog).click(function() {
                    $(this).html("Loading...").attr("disabled", true);
                    $('#dataform').ajaxSubmit({
                        dataType: 'json',
                        url : a.attr('href'),
                        success : function(data){
                            if(data.error==0){
                                //alert(data.success);
                                window.location.reload();
                            }else{
                                //alert(data.error);
                                $(".modal-body", alertDialog).html(data.error);
                                alertDialog.modal("show");
                            }
                        },
                        complete: function(){
                            $(this).html("OK").attr("disabled", false);
                            confirmDialog.modal("hide");
                        }
                    });
                });
                /*var yakin = confirm("Apa anda yakin?");
                 if(yakin){
                 $('#dataform').ajaxSubmit({
                 dataType: 'json',
                 url : a.attr('href'),
                 success : function(data){
                 if(data.error==0){
                 //alert(data.success);
                 window.location.reload();
                 }else{
                 alert(data.error);
                 }
                 }
                 });
                 }*/
                return false;
            });
        }
        if($('.cekboxtarget').length){
            $('.cekboxtarget').click(function(){
                var a = $(this);
                var confirmDialog = $("#confirmDialog");
                var alertDialog = $("#alertDialog");

                confirmDialog.on("hidden.bs.modal", function(){
                    $(".modal-body", confirmDialog).html("");
                });
                alertDialog.on("hidden.bs.modal", function(){
                    $(".modal-body", alertDialog).html("");
                });

                if($('.cekbox:checked').length < 1){
                    $(".modal-body", alertDialog).html("Tidak ada data dipilih");
                    alertDialog.modal("show");
                    //alert('Tidak ada data dipilih');
                    return false;
                }

                $(".modal-body", confirmDialog).html(a.attr("confirm").replace("{count}", $('.cekbox:checked').length));
                confirmDialog.modal("show");
                $(".btn-ok", confirmDialog).click(function() {
                    $('#dataform').attr("action", a.attr('href')).attr("target", "_blank").submit();
                    confirmDialog.modal("hide");
                });
                return false;
            });
        }

        $('a[href="<?=current_url()?>"]').addClass('active');
        $('a[href="<?=current_url()?>"]').closest('li.has-treeview').addClass('menu-open').children('.nav-link').addClass('active');
        //$('li.treeview.active').find('ul').eq(0).show();
        //$('li.treeview.active').find('.fa-angle-left').removeClass('fa-angle-left').addClass('fa-angle-down');

        $(".editor").wysihtml5();
        $("select").not('.no-select2').select2({ width: 'resolve', theme: 'bootstrap4' });
        //$("select").selectpicker();
        $('.datepicker').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            minYear: 1970,
            maxYear: parseInt(moment().format('YYYY'),10),
            locale: {
                format: 'Y-MM-DD'
            }
        });

        /*$(".alert").fadeTo(2000, 500).slideUp(500, function(){
         $(".alert").slideUp(500);
         });*/
        $( ".alert-dismissible" ).fadeOut(3000, function() {
            // Animation complete.
        });
        //iCheck for checkbox and radio inputs
        /*$('input[type="checkbox"], input[type="radio"]').iCheck({
         checkboxClass: 'icheckbox_minimal-blue',
         radioClass: 'iradio_minimal-blue'
         });*/
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass: 'iradio_minimal-blue'
        });

        $('.colorpick').colorpicker();
        $('.colorpick').on('colorpickerChange', function(event) {
            $('.colorpick .fa-square').css('color', event.color.toString());
        });

        $(".money").number(true, 2, '.', ',');
        $(".uang").number(true, 0, '.', ',');

        /*$.ajaxSetup({
         beforeSend: function(xhr) {
         xhr.setRequestHeader('HTTP_X_REQUESTED_WITH', 'xmlhttprequest');
         }
         });*/
    });
</script>