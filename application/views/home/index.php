<?php $this->load->view('header') ?>
    <div class="content pt-3">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card card-default card-default card-filter">
                        <div class="card-header">
                            <h5 class="card-title m-0">Pencarian Pegawai</h5>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                            </div>
                        </div>
                        <div class="card-body" style="display: block">
                            <?=form_open_multipart(site_url('home/index'),array('role'=>'form','id'=>'search-form','class'=>'form-horizontal','method'=>'get'))?>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4">Data maks.</label>
                                        <div class="col-sm-8">
                                            <select name="max" class="form-control no-select2">
                                                <option value="50" <?=!empty($max)&&$max==50?'selected':''?>>50</option>
                                                <option value="100" <?=!empty($max)&&$max==100?'selected':''?>>100</option>
                                                <option value="1000" <?=!empty($max)&&$max==1000?'selected':''?>>1000</option>
                                                <option value="10000" <?=!empty($max)&&$max==10000?'selected':''?>>10000</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4">Urut berdasarkan</label>
                                        <div class="col-sm-8">
                                            <select name="sortby" class="form-control no-select2">
                                                <option value="no" <?=!empty($sortby)&&$sortby=='no'?'selected':''?>>Data Terbaru</option>
                                                <option value="nama" <?=!empty($sortby)&&$sortby=='nama'?'selected':''?>>Nama</option>
                                                <option value="nip" <?=!empty($sortby)&&$sortby=='nip'?'selected':''?>>NIP / NIK</option>
                                                <option value="tgllahir" <?=!empty($sortby)&&$sortby=='tgllahir'?'selected':''?>>Tanggal Lahir</option>
                                                <option value="pangkat-gol-ruang" <?=!empty($sortby)&&$sortby=='pangkat-gol-ruang'?'selected':''?>>Pangkat</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4">Urutan</label>
                                        <div class="col-sm-8">
                                            <select name="sortdir" class="form-control no-select2">
                                                <option value="asc" <?=!empty($sortdir)&&$sortdir=='asc'?'selected':''?>>ASC</option>
                                                <option value="desc" <?=!empty($sortdir)&&$sortdir=='desc'?'selected':''?>>DESC</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 pl-3">
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4">Nama</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" placeholder="Masukkan Kata Kunci..." name="<?=COL_NAMA?>" value="<?=!empty($nama)?$nama:''?>" />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4">NIP / NIK</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" placeholder="Masukkan Kata Kunci..." name="<?=COL_NIP?>" value="<?=!empty($nip)?$nip:''?>" />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4">Unit Kerja</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" placeholder="Masukkan Kata Kunci..." name="<?=COL_UNITKERJA?>" value="<?=!empty($unitkerja)?$unitkerja:''?>" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i>&nbsp;&nbsp;SUBMIT</button>
                                </div>
                            </div>
                            <?=form_close()?>
                        </div>
                    </div>
                    <?php
                    if(!empty($_GET)) {
                        ?>
                        <div class="card card-solid card-result">
                            <div class="card-header">
                                <h5 class="card-title">Hasil Pencarian</h5>
                            </div>
                            <div class="card-body pb-0">
                                <?php
                                if(count($res) > 0) {
                                ?>
                                    <div class="row d-flex align-items-stretch">
                                <?php
                                    foreach($res as $d) {
                                        $foto = MY_IMAGEURL.'pegawai/blank.jpg';
                                        if(!empty($d[COL_NIP]) && is_file(MY_IMAGEPATH.'pegawai/'.$d[COL_NIP].'.jpg')) {
                                            $foto = MY_IMAGEURL.'pegawai/'.$d[COL_NIP].'.jpg';
                                        }
                                        $bgrib = 'bg-danger';
                                        if (strpos(strtolower($d[COL_STATUSPNS]), 'cpns') !== false) {
                                            $bgrib = 'bg-default';
                                        }
                                        else if (strpos(strtolower($d[COL_STATUSPNS]), 'pns') !== false) {
                                            $bgrib = 'bg-success';
                                        }
                                        else {
                                            $bgrib = 'bg-warning';
                                        }
                                        ?>
                                            <div class="col-12 col-sm-6 col-md-4 d-flex align-items-stretch">
                                                <div class="card bg-light">
                                                    <div class="ribbon-wrapper">
                                                        <div class="ribbon <?=$bgrib?>"><?=strtoupper($d[COL_STATUSPNS])?></div>
                                                    </div>
                                                    <div class="card-header border-bottom-0">
                                                        <h5 class="mb-0"><?=$d[COL_NAMA]?></h5>
                                                        <small class="text-muted"><?=strtoupper($d[COL_UNITKERJA])?></small>
                                                    </div>
                                                    <div class="card-body pt-0">
                                                        <div class="row">
                                                            <div class="col-9">
                                                                <!--<h2 class="lead"><b><?=$d[COL_NAMA]?></b></h2>-->
                                                                <!--<p class="text-muted text-sm"><?=$d[COL_JABATAN]?></p>-->
                                                                <ul class="ml-4 mb-0 fa-ul text-muted">
                                                                    <!--<li class="small"><span class="fa-li"><i class="fa fa-lg fa-building"></i></span> <?=$d[COL_UNITKERJA]?></li>-->
                                                                    <li class="small"><span class="fa-li"><i class="fa fa-lg fa-user"></i></span> <?=$d[COL_NIP]?></li>
                                                                    <li class="small"><span class="fa-li"><i class="fa fa-lg fa-globe"></i></span> <?=$d[COL_TEMPATLAHIR]?></li>
                                                                    <li class="small"><span class="fa-li"><i class="fa fa-lg fa-birthday-cake"></i></span> <?=date('d-m-Y', strtotime($d[COL_TGLLAHIR]))?></li>
                                                                    <li class="small"><span class="fa-li"><i class="fa fa-lg fa-black-tie"></i></span> <?=$d[COL_JABATAN]?></li>
                                                                </ul>
                                                            </div>
                                                            <div class="col-3 text-center">
                                                                <img src="<?=$foto?>?x=<?=date('YmdHis')?>" alt="Pas Photo" class="img-circle img-fluid" style="border-radius: 10%">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="card-footer">
                                                        <!--<div class="text-right">
                                                            <a href="#" class="btn btn-sm bg-teal">
                                                                <i class="fas fa-comments"></i>
                                                            </a>
                                                            <a href="#" class="btn btn-sm btn-primary">
                                                                <i class="fas fa-user"></i> View Profile
                                                            </a>
                                                        </div>-->
                                                        <div class="text-right">
                                                            <div class="btn-group">
                                                                <a href="<?=site_url('pegawai/detail/'.$d[COL_NO])?>" class="btn bg-gradient-primary btn-detail"><i class="fa fa-eye"></i></a>
                                                                <a href="<?=site_url('pegawai/edit/'.$d[COL_NO])?>" class="btn bg-gradient-success"><i class="fa fa-pencil"></i></a>
                                                                <a href="<?=site_url('pegawai/delete/'.$d[COL_NO])?>" class="btn bg-gradient-danger"><i class="fa fa-trash"></i></a>
                                                            </div>
                                                            <div class="btn-group">
                                                                <a href="<?=site_url('pegawai/changephoto/'.$d[COL_NO])?>" class="btn btn-outline-info btn-upload"><i class="fa fa-upload"></i></a>
                                                                <button type="button" class="btn btn-outline-info dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                                    <i class="fa fa-print"></i>
                                                                </button>
                                                                <div class="dropdown-menu" role="menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(68px, 38px, 0px);">
                                                                    <a class="dropdown-item" href="<?=site_url('pegawai/cetak-full/'.$d[COL_NO])?>" target="_blank">Full</a>
                                                                    <a class="dropdown-item" href="<?=site_url('pegawai/cetak-partial').'?no='.$d[COL_NO].'&side=depan'?>" target="_blank">Depan</a>
                                                                    <a class="dropdown-item" href="<?=site_url('pegawai/cetak-partial').'?no='.$d[COL_NO].'&side=belakang'?>" target="_blank">Belakang</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php
                                    }
                                    ?>
                                    </div>
                                    <?php
                                } else {
                                    echo '<p>Tidak ada data ditemukan.';
                                }
                                ?>
                            </div>
                        </div>
                        <?php
                    }
                    ?>

                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="uploadDialog" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Upload Foto</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fa fa-close"></i></span>
                    </button>

                </div>
                <div class="modal-body">
                    <form id="upload-form" enctype="multipart/form-data">
                        <div class="form-group row">
                            <label class="control-label col-sm-4">File Photo</label>
                            <div class="col-sm-8">
                                <input type="file" name="userfile" accept="image/jpeg">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary btn-ok">OK</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="detailDialog" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Detail Pegawai</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fa fa-close"></i></span>
                    </button>

                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>
<?php $this->load->view('loadjs') ?>
<script>
    $(document).ready(function() {
        $('.btn-upload').click(function() {
            var href = $(this).attr('href');
            var modal = $("#uploadDialog");
            modal.modal('show');
            $('.btn-ok', modal).click(function() {
                $(this).html("Loading...").attr("disabled", true);
                $('#upload-form').ajaxSubmit({
                    dataType: 'json',
                    url : href,
                    success : function(data){
                        if(data.error==0){
                            //alert(data.success);
                            window.location.reload();
                        }else{
                            alert(data.error);
                        }
                    },
                    complete: function(){
                        $(this).html("OK").attr("disabled", false);
                        modal.modal("hide");
                    }
                });
            });
            return false;
        });

        var detailModal = $("#detailDialog");
        detailModal.on("hidden.bs.modal", function(){
            $(".modal-body", detailModal).html("Loading...");
        });

        $('.btn-detail').click(function() {
            var href = $(this).attr('href');
            $(".modal-body", detailModal).load(href, function() {
                detailModal.modal('show');
            });
            return false;
        });

        <?php
        if(!empty($res)) {
        ?>
        $("button[data-card-widget=collapse]", $('.card-filter')).click();
        <?php
        }
        ?>
    });
</script>
<?php $this->load->view('footer') ?>