
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title><?=!empty($title) ? $title.' | '.$this->setting_web_name : $this->setting_web_name?></title>

    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/fontawesome-free/css/all.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/dist/css/adminlte.min.css">
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/fontawesome-free/css/all.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/tbs/css/font-awesome.min.css" />
    <!-- Ionicons -->
    <link href="<?=base_url()?>assets/tbs/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <link rel="icon" type="image/png" href=<?=MY_IMAGEURL.$this->setting_web_logo?>>

    <!-- Select 2 -->
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/select2/css/select2.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">

    <!-- JQUERY -->
    <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/jQuery/jquery-2.2.3.min.js"></script>
    <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/modernizr/modernizr.js"></script>

    <!-- daterange picker -->
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/daterangepicker/daterangepicker.css">

    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
</head>
<body class="hold-transition layout-top-nav layout-navbar-fixed">
<?php
$ruser = GetLoggedUser();
$displayname = $ruser ? $ruser[COL_NAMALENGKAP] : "Guest";
$displaypicture = MY_IMAGEURL.'user.jpg';
?>
<div class="wrapper">

    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-light navbar-white">
        <div class="container">
            <a href="<?=site_url()?>" class="navbar-brand">
                <img src="<?=MY_IMAGEURL.$this->setting_web_logo?>" alt="Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
                <span class="brand-text font-weight-light"><?=$this->setting_web_name?></span>
            </a>

            <ul class="navbar-nav ml-auto">
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="<?=site_url()?>" class="nav-link">Home</a>
                </li>
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="<?=site_url('user/index')?>" class="nav-link">Users</a>
                </li>
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="<?=site_url('pegawai/add')?>" class="nav-link">Tambah Pegawai</a>
                </li>
                <!--<li class="nav-item dropdown show">
                    <a id="pegawaiSub" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" class="nav-link dropdown-toggle">Tambah Pegawai</a>
                    <ul aria-labelledby="pegawaiSub" class="dropdown-menu border-0 shadow">
                        <li><a href="<?=site_url('pegawai/add-pns/')?>" class="dropdown-item">PNS </a></li>
                        <li><a href="<?=site_url('pegawai/add-honorer/')?>" class="dropdown-item">Non PNS</a></li>
                    </ul>
                </li>-->

                <?php
                if(IsLogin()) {
                    ?>
                    <li class="nav-item dropdown user-menu">
                        <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            <img src="<?=$displaypicture?>" class="user-image img-circle elevation-2" alt="<?=$displayname?>">
                        </a>
                        <ul class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                            <li class="user-header bg-default">
                                <img src="<?=$displaypicture?>" class="img-circle elevation-2" alt="<?=$displayname?>">

                                <p>
                                    <?=$ruser[COL_NAMALENGKAP]?>
                                    <small><?=strtoupper($ruser[COL_LEVEL])?></small>
                                </p>
                            </li>
                            <li class="user-footer">
                                <a href="<?=site_url('user/logout')?>" class="btn btn-default btn-flat float-right">Logout</a>
                            </li>
                        </ul>
                    </li>
                <?php
                } else {
                    ?>
                    <li class="nav-item">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="<?=site_url('user/login')?>" title="Login">
                            <i class="fa fa-sign-in"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                            <li class="user-header bg-default">
                                <p class="text-left" style="margin-bottom: 5px; text-decoration: underline">Login</p>
                                <p class="text-left" style="margin-bottom: 10px; margin-top: 5px;">
                                    <small>Belum punya akun? Silakan <a href="<?=site_url('user/register')?>">daftar</a>.</small>
                                </p>
                                <div>
                                    <?= form_open(site_url('user/login-guest'),array('id'=>'login-form')) ?>
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control form-control-sm" name="<?=COL_USERNAME?>" placeholder="Username" required>
                                        <div class="input-group-append">
                                            <div class="input-group-text">
                                                <span class="fas fa-user"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="input-group mb-3">
                                        <input type="password" class="form-control form-control-sm" name="<?=COL_PASSWORD?>" placeholder="Password" required>
                                        <div class="input-group-append">
                                            <div class="input-group-text">
                                                <span class="fas fa-key"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <?= form_close(); ?>
                                </div>
                            </li>
                            <li class="user-footer">
                                <a href="<?=site_url('user/login')?>" class="btn btn-primary btn-flat">Login</a>
                            </li>
                        </ul>
                    </li>
                <?php
                }
                ?>
            </ul>
        </div>
    </nav>
    <div class="content-wrapper">
