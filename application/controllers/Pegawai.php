<?php
/**
 * Created by PhpStorm.
 * User: Partopi Tao
 * Date: 01/02/2020
 * Time: 14:41
 */

class Pegawai extends MY_Controller {
    function __construct() {
        parent::__construct();
        if(!IsLogin()) {
            redirect('user/login');
        }
    }

    function qrcode($char) {
        $this->load->library('ciqrcode');
        $config['cacheable']	= false; //boolean, the default is true
        $config['cachedir']		= ''; //string, the default is application/cache/
        $config['errorlog']		= ''; //string, the default is application/logs/
        $config['quality']		= true; //boolean, the default is true
        $config['size']			= ''; //interger, the default is 1024
        $config['black']		= array(224,255,255); // array, default is array(255,255,255)
        $config['white']		= array(70,130,180); // array, default is array(0,0,0)

        header("Content-Type: image/png");
        $params['data'] = $char;
        $params['level'] = 'L';
        $params['size'] = 4;
        $params['savename'] = MY_IMAGEPATH.'qr.png';
        $this->ciqrcode->generate($params);
    }

    public function add() {
        $user = GetLoggedUser();
        $data['title'] = "Tambah Pegawai";
        $data['edit'] = FALSE;

        if(!empty($_POST)){
            $data['data'] = $_POST;
            $rec = array(
                COL_NIP => $this->input->post(COL_NIP),
                COL_NAMA => $this->input->post(COL_NAMA),
                COL_NAMAKARTU => $this->input->post(COL_NAMAKARTU),
                COL_STATUSPNS => $this->input->post(COL_STATUSPNS),
                COL_TEMPATLAHIR => $this->input->post(COL_TEMPATLAHIR),
                COL_TGLLAHIR => $this->input->post(COL_TGLLAHIR),
                COL_JENISKELAMIN => $this->input->post(COL_JENISKELAMIN),
                COL_ALAMAT1 => $this->input->post(COL_ALAMAT1),
                COL_PENDIDIKANTERAKHIR => $this->input->post(COL_PENDIDIKANTERAKHIR),
                COL_SEKOLAH => $this->input->post(COL_SEKOLAH),
                COL_JURUSAN => $this->input->post(COL_JURUSAN),
                COL_UNITKERJA => $this->input->post(COL_UNITKERJA),
                COL_JABATAN => $this->input->post(COL_JABATAN),
                'pangkat-gol-ruang' => $this->input->post('pangkat-gol-ruang'),
                COL_TMTCPNS => $this->input->post(COL_TMTCPNS),
                COL_TMTGOL => $this->input->post(COL_TMTGOL),
                COL_TMTJABATAN => $this->input->post(COL_TMTJABATAN),
                COL_MASAKERJA => $this->input->post(COL_MASAKERJA),
                COL_KETERANGAN => $this->input->post(COL_KETERANGAN),
            );
            $this->db->trans_begin();
            try {
                $res = $this->db->insert(TBL_PEGAWAI, $rec);

                $log = array(
                    COL_NAMA => $user[COL_USERNAME],
                    COL_ACTION => 'Tambah Pegawai dengan ID '.$this->input->post(COL_NIP),
                    COL_TGLJAM => date('Y-m-d H:i:s')
                );
                $res2 = $this->db->insert(TBL_ACTION, $log);

                if($res && $res2) {
                    $this->db->trans_commit();
                    redirect('home/index');
                } else {
                    $this->db->trans_rollback();
                    redirect(current_url()."?error=1");
                }
            } catch(Exception $ex) {
                $this->db->trans_rollback();
                redirect(current_url()."?error=1");
            }
        }
        else {
            $this->load->view('pegawai/form', $data);
        }
    }

    function edit($id) {
        $user = GetLoggedUser();

        $rdata = $data['data'] = $this->db->where(COL_NO, $id)->get(TBL_PEGAWAI)->row_array();
        if(empty($rdata)){
            show_404();
            return;
        }
        $data['title'] = "Ubah Pegawai";
        $data['edit'] = TRUE;
        if(!empty($_POST)){
            $data['data'] = $_POST;
            $rec = array(
                COL_NIP => $this->input->post(COL_NIP),
                COL_NAMA => $this->input->post(COL_NAMA),
                COL_NAMAKARTU => $this->input->post(COL_NAMAKARTU),
                COL_STATUSPNS => $this->input->post(COL_STATUSPNS),
                COL_TEMPATLAHIR => $this->input->post(COL_TEMPATLAHIR),
                COL_TGLLAHIR => $this->input->post(COL_TGLLAHIR),
                COL_JENISKELAMIN => $this->input->post(COL_JENISKELAMIN),
                COL_ALAMAT1 => $this->input->post(COL_ALAMAT1),
                COL_PENDIDIKANTERAKHIR => $this->input->post(COL_PENDIDIKANTERAKHIR),
                COL_SEKOLAH => $this->input->post(COL_SEKOLAH),
                COL_JURUSAN => $this->input->post(COL_JURUSAN),
                COL_UNITKERJA => $this->input->post(COL_UNITKERJA),
                COL_JABATAN => $this->input->post(COL_JABATAN),
                'pangkat-gol-ruang' => $this->input->post('pangkat-gol-ruang'),
                COL_TMTCPNS => $this->input->post(COL_TMTCPNS),
                COL_TMTGOL => $this->input->post(COL_TMTGOL),
                COL_TMTJABATAN => $this->input->post(COL_TMTJABATAN),
                COL_MASAKERJA => $this->input->post(COL_MASAKERJA),
                COL_KETERANGAN => $this->input->post(COL_KETERANGAN),
            );
            $this->db->trans_begin();
            try {
                $res = $this->db->where(COL_NO, $id)->update(TBL_PEGAWAI, $rec);

                $log = array(
                    COL_NAMA => $user[COL_USERNAME],
                    COL_ACTION => 'Edit Pegawai dengan ID '.$rdata[COL_NIP].($rdata[COL_NIP] != $this->input->post(COL_NIP) ? ' menjadi '. $this->input->post(COL_NIP) : ''),
                    COL_TGLJAM => date('Y-m-d H:i:s')
                );
                $res2 = $this->db->insert(TBL_ACTION, $log);

                if($res || $res2) {
                    $this->db->trans_commit();
                    redirect('home/index');
                } else {
                    $this->db->trans_rollback();
                    redirect(current_url()."?error=1");
                }
            } catch(Exception $ex) {
                $this->db->trans_rollback();
                redirect(current_url()."?error=1");
            }
        }
        else {
            $this->load->view('pegawai/form', $data);
        }
    }

    function changephoto($id) {
        $rpegawai = $this->db->where(COL_NO, $id)->get(TBL_PEGAWAI)->row_array();
        if(empty($rpegawai)) {
            ShowJsonError('Data pegawai tidak valid.');
            return;
        }

        $config['upload_path'] = MY_IMAGEPATH.'pegawai/';
        $config['allowed_types'] = UPLOAD_ALLOWEDTYPES;
        $config['max_size']	= 512000;
        $config['max_width']  = 4000;
        $config['max_height']  = 4000;
        $config['overwrite'] = TRUE;
        $config['remove_spaces'] = FALSE;
        $config['file_name'] = $rpegawai[COL_NIP].'.'.pathinfo($_FILES['userfile']['name'], PATHINFO_EXTENSION);

        $this->load->library('upload',$config);
        if(!empty($_FILES['userfile']['name'])) {
            if(!$this->upload->do_upload()){
                ShowJsonError('Gagal mengupload foto.');
                return;
            }

            $fileData = $this->upload->data();
            //ShowJsonError($fileData['file_name'].'-----'.$config['file_name']);
            ShowJsonSuccess('Berhasil');
            return;
        }
    }

    function detail($id) {
        $res = $data['res'] = $this->db
            ->where(COL_NO, $id)
            ->get(TBL_PEGAWAI)
            ->row_array();

        if(empty($res)) {
            echo 'Data pegawai tidak valid.';
            return;
        }

        $this->load->view('pegawai/view_partial', $data);
    }

    function delete($id){
        $rpegawai = $this->db->where(COL_NO, $id)->get(TBL_PEGAWAI)->row_array();
        if(empty($rpegawai)) {
            redirect('home/index');
        }

        $this->db->delete(TBL_PEGAWAI, array(COL_NO => $id));
        if(!empty($rpegawai[COL_NIP]) && is_file(MY_IMAGEPATH.'pegawai/'.$rpegawai[COL_NIP].'.jpg')) {
            unlink(MY_IMAGEPATH.'pegawai/'.$rpegawai[COL_NIP].'.jpg');
        }
        redirect('home/index');
    }

    function cetak_full($id) {
        $res = $data['res'] = $this->db
            ->where(COL_NO, $id)
            ->get(TBL_PEGAWAI)
            ->row_array();

        if(empty($res)) {
            show_404();
            return;
        }

        $this->load->view('pegawai/view_cetak_full', $data);
    }

    function cetak_partial() {
        $no = $this->input->get(COL_NO);
        $data['side'] = $this->input->get('side');
        $res = $data['res'] = $this->db
            ->where(COL_NO, $no)
            ->get(TBL_PEGAWAI)
            ->row_array();

        if(empty($res)) {
            show_404();
            return;
        }

        $html = $this->load->view('pegawai/view_cetak_partial', $data, true);
        $this->load->library('Topdf');
        $mpdf = new \Topdf('c', array(55,85), '', '', 0, 0, 0, 0, 0, 0);
        $mpdf->SetTitle('Cetak Kartu | '.$res[COL_NIP]);
        $mpdf->SetDisplayMode('fullpage');
        $mpdf->simpleTables = true;
        $mpdf->packTableData = true;
        $keep_table_proportions = TRUE;
        $mpdf->shrink_tables_to_fit=1;
        $mpdf->WriteHTML($html);
        $mpdf->Output('Kartu ID '.$res[COL_NIP].'.pdf', 'I');
    }
}
