<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {
    function __construct() {
        parent::__construct();
        $this->load->library('session');
    }

    public function index()
    {
        if(!IsLogin()) {
            redirect('user/login');
        }
        $data['title'] = 'Home';
        if(!empty($_GET)) {
            $data[COL_NAMA] = $nama = $this->input->get(COL_NAMA);
            $data[COL_NIP] = $nip = $this->input->get(COL_NIP);
            $data[COL_UNITKERJA] = $unit = $this->input->get(COL_UNITKERJA);
            $data['max'] = $limit = $this->input->get("max");
            $data['sortby'] = $orderby = $this->input->get("sortby");
            $data['sortdir'] = $orderdir = $this->input->get("sortdir");

            if(!empty($nama)) $this->db->like(COL_NAMA, $nama);
            if(!empty($nip)) $this->db->like(COL_NIP, $nip);
            if(!empty($unit)) $this->db->like(COL_UNITKERJA, $unit);
            $data['res'] = $this->db
                ->order_by($orderby, $orderdir)
                ->limit($limit)
                ->get(TBL_PEGAWAI)
                ->result_array();
        }
        $this->load->view('home/index', $data);
    }

    function _404() {
        $this->load->view('home/error');
    }
}
