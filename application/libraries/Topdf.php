<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
include_once FCPATH.'vendor/autoload.php';
class Topdf {

    public $param;
    public $pdf;

    public function __construct($mode='',$format='A4',$default_font_size=0,$default_font='',$mgl=15,$mgr=15,$mgt=16,$mgb=16,$mgh=9,$mgf=9, $orientation='P')
    {
        //$this->param =$param;
        $this->pdf = new \Mpdf\Mpdf(array(
          'mode'=>$mode,
          'format'=>$format,
          'orientation'=>$orientation
        ));
    }

}
